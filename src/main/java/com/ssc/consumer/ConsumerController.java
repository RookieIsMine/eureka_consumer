package com.ssc.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ConsumerController {
    @Autowired
    private RestTemplate restTemplate;
    @GetMapping("/consumer/callHello")
    public String callHello(){
        return restTemplate.getForObject("http://localhost:8055/user/hello",String.class);
    }

    @GetMapping("/consumer/callHello2")
    public String callHello2(){
        return restTemplate.getForObject("http://eureka-user-provide/user/hello",String.class);
    }

}
