package com.ssc.consumer;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

@Configuration
public class BeanConfiguration {
   @Bean
   @LoadBalanced
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }


}
